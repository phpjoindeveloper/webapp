# WEBApp
## アプリの起動方法
### 前提条件
- dockerおよびdocker-composeがインストールされている事

### 起動手順
1. docker-compose.ymlが存在するディレクトリに移動
2. docker-compose buildコマンドをターミナルに入力
3. docker-compose upコマンドをターミナルに入力
